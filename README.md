GEM
===========================
A simple match 3 game using [game closure](http://gameclosure.com).

Gameplay
--------
- A standard match 3 with a tetris like twist.
- Goal is to get as much score as possible before the game is over.
- Align at least 3 similiar tiles in a row or col.
- When there is a matched, all the similiar tiles adjacent to matched tiles will be destroyed.
- Get a power on every 3 moves.
- Power will destory all the simliar tiles with the highest count.
- On every 5 seconds interval, new row will be added at the bottom.
- When you hit over 200 score, add new row interval will be reduce to 4, to 3 in the case of over 300 score.
- When the tile hit the top row of the board, you lose (similiar to tetris).

Prerequisite
------------
- Make sure you have game closure devkit installed.

Install
-------
- Clone the project and run `devkit install`.

Run
---
- Run `devkit serve` command.
- Default url will be `localhost:9200`, select the project and click the `simulate`

Author
-------
- [Max Wan](mailto:maxwkh@gmail.com)

Credit
------
 - Images: [Blackstrom](http://blackstormlabs.com)
 - Sounds: [Music is VFR](http://musicisvfr.com)

Demo
----
Here are the demos.
- [mobile](https://maxwkh.bitbucket.io/gem/browser/mobile)
- [desktop](https://maxwkh.bitbucket.io/gem/browser/desktop)