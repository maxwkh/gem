Relection
=========
My approach and encounters when making this game in 7 days (after working hours).


Challenges
----------
1. Hardest part is trying to pick up game closure API in few hours. There are some examples on the official website, it is easy to understand. However, not all of them works properly. For an example, the `ui.widget.GridView` doesn't work as expected.
2. Trying to make sure there is no bug when player is swapping a tile while the board adds a new row of tiles.
3. Game was not really interesting after the gameplay was implemented, however adding new sounds does help improving the overall feeling.
4. Game looks dull and too static. Adding some effect to UI element, such as animation on button makes the game looks more alive.
5. Creating art work (I am bad at it), so I tried to reuse/alter some of the existing assets to get a similiar/consistent feeling with the reset of the game.


Game logic / algorithm
---------------------------------
1. Board is created when the app start. Tile position and partile effect engine will be calculated and created at this point.
2. Two rows of tiles will be added on to the board once the game starts.
3. Swap feature is detected by clicking on the first tile followed by the swipe direction. Direction is calculated from the first tile position and the last touch end position.
4. When there is a valid swipe action, swap the target tile with its adjacent tile (based on the swipe direction) and revert the swap in the case of invalid swipe action.
5. Recusively loop through the tiles adjacent to the target tile (frist targeted tile) and returns all the similiar tile type.
6. Destroy all the similar tiles. Particle effect will be triggered for each relevant tiles. Each tile have its own particle engine and will be reused.
7. Next is to fill up the gap, the lowest row empty slot position will be calculated for each columns.
8. Loop through each columns. Then for each row, starting looping from the lowest row for an empty slot calculated earlier and start moving all the tiles above to fill in the gaps below until there is no more gap between tiles.
9. At a specified interval, a new row of tiles will be added at the lowest end of the board and push all the tiles up a row.
10. After moving all the tiles, check the frist row of the board to make sure there are no tiles or it is game over.
11. Board is not destroyed after game over, only destroy the tiles.


Game play
---------
- Wanted to make a different match 3 game from Candy Crush, so I came up with a combination of regular match 3 and tetris gameplay.
- The goal is to get as much points as possible before it is game over.
- Destroy tiles with regular match 3 method and end the game like tetris the moment a tile hit the highest grid on the board.
- You will get a power on every 3 moves.
- Triggering a power will destory all similiar tiles with the highest count on the board.


Improvement on Game Closure (GC)
---------------------------
- Example should always works. Since GC user base is not as big as other engine (e.g: PIXI), there are less help/reference you can get quickly online.
- It would be nice to have a tools to simulate the particle system engine (same thing for image slices).
- In a regular Javascript development, you can store data into a window object or `store in temporary variable via chrome`, it would be nice to have something simliar for GC to inspect some data on the fly.


Possible improvement on the game/code
------------------------------
- Generate tile particle engine for a group of tiles (not on every individual tile), particles position can be randomised based on the allowable area of the tiles group.
- Better organisation of code. E.g: in a smaller chuck (more files) etc.
- Inform the user on the game's goal and add visual cue to indicate "next add row action" and "obtained power".
- Preload all assets before the game starts.
- Tweet the game balance etc.


After thought
-------------
- A plus on GC is the way to simulate different phone screen instantaneously (it would be nice to have a larger phones database, but it is good enough at the moment).
- The ability to point to an element like a DOM is convenient.
- This game "GEM" still requires a lot of work to make it feel good and interesting, but it would be awesome if I managed to improve and publish it on Facebook Messenger game.