import AudioManager;

exports.sound = null;

/* Initialize the audio files if they haven't been already.
 */
exports.getSound = function () {
  if (!exports.sound) {
    exports.sound = new AudioManager({
      path: 'resources/sounds',
      files: {
        title: {
          volume: 0.5,
          background: true,
          loop: true
        },
        levelmusic: {
          volume: 0.5,
          background: true,
          loop: true
        },
        tileDestroy: {
          background: false
        },
        push: {
          background: false,
          volume: 0.3,
        },
        power: {
          background: false
        },
        swap: {
          background: false
        },
        button: {
          background: false
        },
        gameover: {
          background: false
        }
      }
    });
  }
  return exports.sound;
};