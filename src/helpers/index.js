import animate;
import src.config as Config;
import event.Callback as Callback;


/**
 * Move tile from a postion to other.
 * @param {Tile} tile
 * @param {object} from - From position
 * @param {number} [from.x] - From position x.
 * @param {number} [from.y] - From position y.
 * @param {object} to - To position
 * @param {number} [to.x] - To position x.
 * @param {number} [to.y] - To position y.
 * @param {callback} [cb]
 */
function animateMoveTile(tile, from, to, cb) {
  // animate move
  var animSpeed = Config.animation.speed.tileMove;

  animate(tile)
  .now(from, animSpeed)
  .then(to, animSpeed)
  .then(cb);
}


/**
 * Animate swap between two tiles, trigger callback on finished.
 * @param {Tile} tile1 - Tile 1.
 * @param {Tile} tile2 - Tile 2.
 * @param {callback} [cb]
 */
function animateSwap(tile1, tile2, cb) {
  var tile1Pos = tile1.getPosition(tile1.getSuperview());
  var tile2Pos = tile2.getPosition(tile2.getSuperview());

  var animSpeed = Config.animation.speed.tileSwap;
  var count = 0;

  // both tiles finished animate, return
  var callback = new Callback();
  callback.run(cb);

  animate(tile1)
  .then({ x: tile1Pos.x, y: tile1Pos.y }, animSpeed)
  .then({ x: tile2Pos.x, y: tile2Pos.y }, animSpeed)
  .then(callback.chain());

  animate(tile2)
  .then({ x: tile2Pos.x, y: tile2Pos.y }, animSpeed)
  .then({ x: tile1Pos.x, y: tile1Pos.y }, animSpeed)
  .then(callback.chain());
}


/**
 * Generate tile ID from row and col value.
 * @param {number} row - Row.
 * @param {number} col - Col.
 * @returns {string} Tile ID.
 */
function generateId(row, col) {
  return row + '_' + col;
}


/**
 * Get a tile neighbouring tile info.
 * @param {Tile} tile - Tile.
 * @param {string} direction - Neighbour direction (right, left, top, bottom). Default to right.
 * @param {number} offset - Neighbour offset from direction. Default to 1.
 * @returns {string} Neighbour tile ID.
 */
function getTileNeighbourTileId(tile, direction, offset) {
  offset = offset || 1;
  direction = direction || 'right';

  var neighbourTileID;

  switch (direction) {
    case 'right':
      neighbourTileID = generateId(tile.row, tile.col + offset);
    break;
    case 'left':
      neighbourTileID = generateId(tile.row, tile.col - offset);
    break;
    case 'top':
      neighbourTileID = generateId(tile.row - offset, tile.col);
    break;
    case 'bottom':
      neighbourTileID = generateId(tile.row + offset, tile.col);
    break;
  }

  return neighbourTileID;
}


/**
 * Get the gemId of the highest count from a list of tiles.
 * @param {object} tiles - Map of tileId to tile of a board.
 * @returns {string} Gem Id.
 */
function getTypeWithHighestCount(tiles) {
  var tileCount = {};

  var highestCount = 0;
  var highestGemId = undefined;

  for (var tileId in tiles) {
    if (!tiles.hasOwnProperty(tileId)) {
      continue;
    }

    var tile = tiles[tileId];
    var gemId = tile && tile.gemId;

    if (!gemId) {
      continue;
    }

    if (!tileCount[gemId]) {
      tileCount[gemId] = 0;
    }

    tileCount[gemId] += 1;

    if (tileCount[gemId] > highestCount) {
      highestGemId = gemId;
      highestCount = tileCount[gemId];
    }
  }

  return highestGemId;
};


/**
 * Extract a list of tiles by gem Id from a list of tiles.
 * @param {object} tiles - Map of tileId to tile of a board.
 * @param {string} gemId - Gem Id.
 * @returns {Tile[]} An array of tiles with the same gem Id.
 */
function extractTilesByGemId(tiles, gemId) {
  var list = [];

  for (var tileId in tiles) {
    if (tiles.hasOwnProperty(tileId)) {
      var tile = tiles[tileId];

      if (tile && tile.gemId === gemId) {
        list.push(tile);
      }
    }
  }

  return list;
}


exports = {
  animateMoveTile: animateMoveTile,
  animateSwap: animateSwap,
  generateId: generateId,
  getTileNeighbourTileId: getTileNeighbourTileId,
  getTypeWithHighestCount: getTypeWithHighestCount,
  extractTilesByGemId: extractTilesByGemId
};