exports = {
  animation: {
    speed: {
      tileAppear: 100,
      tileDisappear: 100,
      tileMove: 100,
      tileSwap: 100
    }
  },
  particle: {
    tileEffectTTL: 200
  }
};