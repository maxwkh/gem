import animate;
import device;
import ui.widget.ButtonView as ButtonView;
import ui.ImageScaleView as ImageScaleView;
import src.helpers.sounds as sounds;

exports = Class(ImageScaleView, function (supr) {
  this.init = function (opts) {
    opts = merge(opts, {
      scaleMethod: 'cover',
      layout: 'box',
      x: 0,
      y: 0,
      image: "resources/images/ui/title.png"
    });

    supr(this, 'init', [opts]);

    var buttonWidth = device.width / 2;
    var buttonHeight = device.height * 0.2;
    buttonHeight = buttonHeight > 200 ? 200 : buttonHeight;

    this.sound = sounds.getSound()

    var that = this;

    var startBtn = new ButtonView({
      superview: this,
      width: buttonWidth,
      height: buttonHeight,
      x: (device.width - buttonWidth) / 2,
      y: device.height * 0.7,
      title: 'START GAME',
      text: {
        width: buttonWidth * 0.8,
        x: buttonWidth * 0.1,
        color: '#ffffff',
        shadowColor: '#000000',
        shadowWidth: buttonWidth * 0.8 * 0.01
      },
      images: {
        up: 'resources/images/ui/button.png',
        down: 'resources/images/ui/buttonDown.png'
      },
      on: {
        up: function () {
          that.sound.play('button');
          that.emit('startGame');
        }
      },
      anchorX: buttonWidth / 2,
      anchorY: buttonHeight / 2,
      scale: 0
    });

    // animate
    function buttonAnimation() {
      animate(startBtn)
      .now({ scale: 1 })
      .then({ scale: 0.98 })
      .then(buttonAnimation);
    }

    // hide start button on start
    animate(startBtn)
    .wait(2000)
    .now({ scale: 0 })
    .then({ scale: 1 })
    .then(function () {
      buttonAnimation();
    });
   };
});