import animate;
import device;
import src.components.Board as Board;
import src.components.GameOver as GameOver;
import src.components.PowerButton as PowerButton;
import src.components.ScoreBoard as ScoreBoard;
import ui.ImageScaleView as ImageScaleView;
import ui.View as View;
import src.helpers.sounds as sounds;

exports = Class(View, function (supr) {
  var updateInterval = 5; // in sec

  this.init = function (opts) {
    supr(this, 'init', [opts]);

    this.bgBoardPowerBtnSetup();
    this.scoreBoardSetup();
    this.gameOverSetup();

    this.isGameOver = true;

    this.sound = sounds.getSound();

    var that = this;

    // start the game
    this.on('start', function () {
      that.board.resetBoard();
      that.setupTileGenerationLogic();

      that.sound.stop('title');
      that.sound.play('levelmusic');

      that.isGameOver = false;
    });
  };


  /**
   * Setup background, board and power button.
   */
  this.bgBoardPowerBtnSetup = function () {
     // background creation

    this.bg = new ImageScaleView({
      superview: this,
      scaleMethod: 'cover',
      layout: 'box',
      image: 'resources/images/ui/background.png'
    });

    this.board = new Board({
      superview: this.bg
    });

    this.powerBtn = new PowerButton({
      superview: this.bg,
      board: this.board
    });
  };


  /**
   * Setup score board.
   */
  this.scoreBoardSetup = function () {
    var scoreBoard = new ScoreBoard({
      superview: this.bg
    });


    // update info from board
    this.board.on('updateScore', function (score) {
      scoreBoard.updateScore(score);

      if (score > 200) {
        updateInterval = 4;
      } else if (score > 300) {
        updateInterval = 3;
      }
    });

    var that = this;

    this.board.on('updatePower', function (val) {
      if (val === 0) {
        that.powerBtn.resetValue();
        return;
      }

      that.powerBtn.incPowerCount();
    });
  };


  /**
   * Setup game over UI assets.
   */
  this.gameOverSetup = function () {
    // game over
    var gameOverText = new GameOver({
      superview: this.bg
    });

    var that = this;

    this.board.on('gameOver', function () {
      that.isGameOver = true;

      that.sound.stop('levelmusic');
      that.sound.play('gameover');

      gameOverText.appear();

      setTimeout(function () {
        gameOverText.continueText.show();
        gameOverText.canContinue = true;
      }, 2000);
    });


    gameOverText.on('InputSelect', function () {
      if (gameOverText.canContinue) {
        gameOverText.disappear();
        gameOverText.canContinue = false;
        gameOverText.continueText.hide();

        that.sound.stop('gameover');
        that.emit('end');
      }
    });
  };


  /**
   * Setup board's tiles generation logic.
   */
  this.setupTileGenerationLogic = function () {
    var board = this.board;
    board._generateTilesRowFromBottom();
    board._generateTilesRowFromBottom();
  };



  /**
   * New row generation.
   */
  this.elapsedTime = 0;
  this.addRowCount = 0;
  this.isAddedRow = true;

  this.tick = function (dt) {
    this.sound.stop('title');

    if (this.isGameOver) {
      return;
    }

    if (this.board.interactive) {
      // only increment count if added a row
      if (this.isAddedRow) {
        this.elapsedTime += dt;
      } else {
        // add row now if failed to add last row
        this.board._generateTilesRowFromBottom();
        this.isAddedRow = true;
      }
    }

    // add row on every update interval
    if (((this.elapsedTime / 1000) >> 0) > this.addRowCount + updateInterval) {
      this.addRowCount += updateInterval;

      // set add row state to failure
      this.isAddedRow = false;

      // add row only if board is in interactive mode and set row state to success
      if (this.board.interactive) {
        this.board._generateTilesRowFromBottom();
        this.isAddedRow = true;
      }
    }
  };
});
