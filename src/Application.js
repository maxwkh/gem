import device;
import ui.StackView as StackView;
import src.TitleScreen as TitleScreen;
import src.GameScreen as GameScreen;
import src.helpers.sounds as sounds;

exports = Class(GC.Application, function () {
  this.initUI = function () {
    var titlescreen = new TitleScreen();
    var gamescreen = new GameScreen();

    var rootView = new StackView({
      superview: this,
      width: device.width,
      height: device.height
    });

    rootView.push(titlescreen);

    this.sound = sounds.getSound();
    this.sound.play('title');

    var that = this;

    titlescreen.on('startGame', function () {
      that.sound.stop('title');

      rootView.push(gamescreen);
      gamescreen.emit('start');
    });

    gamescreen.on('end', function () {
      rootView.pop();
      that.sound.play('title');
    });
  };
});
