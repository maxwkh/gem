import animate;
import device;
import ui.ImageScaleView as ImageScaleView;
import ui.TextView as TextView;
import ui.View as View;

exports = Class(View, function (supr) {
  this.init = function (options) {
    options = options || {};

    if (!options.superview) {
      return console.log('ERROR Please supply a superview during the gameover creation');
    }

    var width = device.width * 0.8;
    var height = device.height * 0.65;

    options = merge(options, {
      width: device.width,
      height: device.height,
      x: (device.width - width) / 2,
      anchorX: width / 2,
      scaleY: 0
    });

    supr(this, 'init', [options]);

    this.hide();

    var borderWidth = device.width * 0.05;

    var gameOverBoard = new ImageScaleView({
      superview: this,
      image: 'resources/images/ui/scoreBoard.png',
      width: width,
      height: height,
      scaleMethod: '9slice',
      sourceSlices: {
        horizontal: { left: 20, center: 200, right: 20 },
        vertical: { top: 20, middle: 200, bottom: 20 }
      },
      destSlices: {
        horizontal: { left: borderWidth, right: borderWidth },
        vertical: { top: device.height * 0.3, bottom: borderWidth }
      }
    });

    var gameOverText = new TextView({
      superview: this,
      text: 'Game Over!',
      x: width * 0.1,
      y: height * 0.55,
      width: width * 0.8,
      height: device.height * 0.3,
      color: '#ffffff',
      shadowColor: '#000000',
      fontWeight: 'bold',
      shadowWidth: width * 0.01
    });

    var gameOverTextDimensions = gameOverText.getPosition(this);

    this.continueText = new TextView({
      superview: this,
      text: 'Tap to continue!',
      x: width * 0.3,
      y: height * 0.78,
      width: width * 0.4,
      height: height * 0.2,
      color: '#ffffff',
      fontWeight: 'bold',
    });
    this.continueText.hide();
  };


  /**
   * Animate and show game over.
   */
  this.appear = function () {
    this.show();

    var anim = animate(this);

    anim
    .now({ scaleY: 0 })
    .then({ scaleY: 1 });

    clearInterval(this.interval);
    this.interval = setInterval(function () {
      anim
      .now({ scaleY: 1 })
      .then({ scaleY: 0.98 }, 1000);
    }, 1500);
  };


  /**
   * Animate and hide game over.
   */
  this.disappear = function () {
    if (this.isDisappering) {
      return;
    }

    this.isDisappering = true;

    clearInterval(this.interval);

    var that = this;

    animate(this)
    .now({ scaleY: 1 })
    .then({ scaleY: 0 })
    .then(function () {
      that.hide();
      that.isDisappering = false;
    });
  };
});