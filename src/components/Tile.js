import animate;
import src.config as Config;
import ui.ImageView as ImageView;

var gemTypeCount = 5;

exports = Class(ImageView, function (supr) {
  this.init = function (options) {
    options = options || {};

    if (!options.superview) {
      return console.log('ERROR Please supply a superview during the tile creation');
    }

    this.scale = options.scale || 1;

    options = merge(options, {
      centerAnchor: true,
      layout: 'box',
      scale: this.scale,
      zIndex: 10
    });

    supr(this, 'init', [options]);

    this.id = options.id;
    this.row = options.row;
    this.col = options.col;

    this.randomisedTile();
  };


  /**
   * Randomise tile type.
   */
  this.randomisedTile = function () {
    var gemId = (Math.random() * gemTypeCount + 1) >> 0;
    var assetName = 'gem_0' + gemId;

    // update gemId for type check
    this.gemId = gemId;

    this.updateOpts({
      image: 'resources/images/gems/' + assetName + '.png',
    });
  };


  /**
   * Make tile disappear.
   * @param {callback} cb
   */
  this.disappear = function (cb) {
    var animSpeed = Config.animation.speed.tileDisappear;

    animate(this.style)
    .now({ scale: this.scale }, animSpeed)
    .then({ scale: 0 }, animSpeed)
    .then(cb);
  };


  /**
   * Make tile appear.
   * @param {callback} cb
   */
  this.appear = function (cb) {
    var animSpeed = Config.animation.speed.tileAppear;

    animate(this.style)
    .now({ scale: 0 }, animSpeed)
    .then({ scale: this.scale }, animSpeed)
    .then(cb);
  };
});
