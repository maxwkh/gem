import animate;
import device;
import event.Callback as Callback;
import src.helpers.index as Helpers;
import src.components.Tile as Tile;
import src.components.TileEffect as TileEffect;
import src.helpers.sounds as sounds;
import ui.View as View;

exports = Class(View, function (supr) {
  /**
   * Initialisation.
   * @param {object} [options] - The usual View class option.
   */
  this.init = function (options) {
    options = options || {};

    if (!options.superview) {
      return console.log('ERROR Please supply a superview during the board creation');
    }

    // default values
    var cols = this.cols = options.cols || 9;
    var rows = this.rows = options.rows || 9;

    // calculate dimensions based on device screen size
    var boardMargin = device.width * 20 / 576; // 20% of the bg width is the board margin
    var boardWidth = this.boardWidth = device.width - boardMargin * 2;
    var boardHeight = boardWidth / cols * rows;

    // half board height to screen width %, value calculated from bg asset
    var posTopOffsetFromHalfBoard = 200 / 576;

    options = merge(options, {
      x: boardMargin,
      y: device.height / 2 - device.width * posTopOffsetFromHalfBoard, // center of the screen - offset
      width: boardWidth,
      height: boardHeight * 1.2
    });

    supr(this, 'init', [options]);

    this.resetBoard();
    this._generateTiles(true);
    this._setupInput();

    this.on('validateSwap', validateSwap);
    this.on('matched', matched);
    this.on('resetStates', resetStates);

    this.sound = sounds.getSound();
  };


  // properties

  this.tilesGridPos = {}; // tile position on the board in pixel
  this.tilesEffect = {};

  /**
   * Reset interaction states.
   */
  function resetStates() {
    this._startTile = undefined;
    this._chainCount = 0;
    // can interact with board state
    this.interactive = true;

    // reset position if it is off
    var tiles = this.tiles;

    for (var tileId in tiles) {
      if (tiles.hasOwnProperty(tileId)) {
        var tile = tiles[tileId];

        if (tile) {
          tile.updateOpts(this.getTilePixelPosById(tile.id));
        }
      }
    }
  }


  /**
   * Reset board state.
   * @param {boolean} keepOldTiles - Keep or destroy old tiles, default to false.
   */
  this.resetBoard = function (keepOldTiles) {
    this.tiles = this.tiles || {}; // a map of tiles for look up by id


    if (!keepOldTiles) {
      var tiles = this.tiles;

      for (var tileId in tiles) {
        if (tiles.hasOwnProperty(tileId)) {
          var tile = tiles[tileId];

          if (tile) {
            tile.removeFromSuperview()
          }

          this.tiles[tileId] = undefined;
        }
      }
    }

    this.totalScore = 0;
    this.totalMoves = 0;

    this.emit('updatePower', 0);
    this.emit('updateScore', this.totalScore);

    bind(this, resetStates)();
  };


  /**
   * Validate swap and animate on valid.
   * @param {string} direction - Swap direction.
   */
  function validateSwap(direction) {
    var tile1 = this._startTile;

    if (!tile1) {
      this.emit('resetStates');
      return;
    }

    var tile2Id = Helpers.getTileNeighbourTileId(tile1, direction, 1);
    var tile2 = this.getTileById(tile2Id);

    if (!tile1 || !tile2) {
      this.emit('resetStates');
      return;
    }

    this.swapTileIds(tile1, tile2);

    var tile1Matched = this.haveMatch(tile1);
    var tile2Matched = this.haveMatch(tile2);

    var that = this;

    this.sound.play('swap');

    // animate swap
    Helpers.animateSwap(tile1, tile2, function () {
      // no match, animate revert swap
      if (!tile1Matched && !tile2Matched) {
        that.sound.play('swap');

        Helpers.animateSwap(tile1, tile2, function () {
          that.swapTileIds(tile1, tile2);

          that.emit('resetStates');
        });

        return;
      }

      that.emit('matched', [
        {
          tile: tile1,
          haveMatch: tile1Matched
        },
        {
          tile: tile2,
          haveMatch: tile2Matched
        }
      ]);
    });
  }

  /**
   * There is a match, grab similiar and destroy tiles.
   * @param {object[]} tilesInfo - An array of tileInfo.
   * @param {object} tileInfo - Tile info.
   * @param {Tile} tileInfo.tile - Tile.
   * @param {boolean} tileInfo.haveMatch - Do tile have match flag.
   * @param {boolean} noPower - Exlclude power reward.
   */
  function matched(tilesInfo, noPower) {
      var matchedTiles = [];

      for (var i = 0; i < tilesInfo.length; i += 1) {
        var tileInfo = tilesInfo[i];

        if (tileInfo.haveMatch) {
          // get all matched tiles
          this._populateListWithSimiliarNeighbouringTiles(matchedTiles, tileInfo.tile);
        }
      }

      if (matchedTiles.length) {
        this.destroyMatchedTilesAndCheckCombo(matchedTiles, noPower);
        return;
      }

      this.emit('resetStates');

      if (noPower) {
        return;
      }

      this.totalMoves += 1;

      if (!(this.totalMoves % 3)) {
        this.emit('updatePower');
      }
  }


  this.destroyMatchedTilesAndCheckCombo = function (matchedTiles, noPower) {
    var callback = new Callback();
    var that = this;

    // destroy tiles
    matchedTiles.forEach(function (tile) {
      that.destroyTile(tile, callback.chain());
    });

    this.sound.play('tileDestroy');

    // destroyed tiles, drop tiles
    callback.run(function () {
      that._dropTilesAndCheckComboMatch(matchedTiles, noPower);
    });
  };


  /**
   * Get tile by tile ID.
   * @param {string} tileId - Tile ID.
   * @returns {Tile|undefined} Tile if available, else undefined.
   */
  this.getTileById = function (tileId) {
    return this.tiles[tileId];
  };


  /**
   * Get tile by tile's row and col.
   * @param {number} row - Tile row.
   * @param {number} col - Tile col.
   * @returns {Tile|undefined} Tile if available, else undefined.
   */
  this.getTileByRowCol = function (row, col) {
    var tileId = Helpers.generateId(row, col);
    return this.getTileById(tileId);
  };


  /**
   * Get tile on board pixel position by tile ID.
   * @param {string} tileId - Tile ID.
   * @returns {object} Position map of x and y.
   */
  this.getTilePixelPosById = function (tileId) {
    return this.tilesGridPos[tileId];
  };


  /**
   * Get tile on board pixel position tile's row and col.
   * @param {number} row - Tile row.
   * @param {number} col - Tile col.
   */
  this.getTilePixelPosByRowCol = function (row, col) {
    var tileId = Helpers.generateId(row, col);
    return this.tilesGridPos[tileId];
  };


  /**
   * Populate board with tiles.
   * @param {boolean} onlyPosition - Generate only tile on board position.
   */
  this._generateTiles = function (onlyPosition) {
    // tiles generation

    var cols = this.cols;
    var rows = this.rows;

    var gridWidth = this.gridWidth = this.boardWidth / cols;
    var gridHeight = this.gridHeight = gridWidth;

    // make the tile assets smaller than the board grid dimensions.
    var tileWidth = this.tileWidth = gridWidth * 0.95;
    var tileHeight = this.tileHeight = gridHeight * 0.95;

    if (!onlyPosition) {
      this.setMaxListeners(cols * rows * 2);
    }

    for (var row = 0; row < rows; row += 1) {
      for (var col = 0; col < cols; col += 1) {
        var tileId = Helpers.generateId(row, col);
        var pos = {
          x: col * gridWidth + gridWidth * 0.05,
          y: row * gridHeight + gridHeight * 0.05
        };

        // store position in the map
        this.tilesGridPos[tileId] = pos;

        this.tilesEffect[tileId] = new TileEffect({
          superview: this,
          x: pos.x,
          y: pos.y,
          width: tileWidth * 1,
          height: tileHeight * 1
        });

        if (onlyPosition) {
          continue;
        }

        // store tile reference in the map
        var tile = new Tile({
          id: tileId,
          row: row,
          col: col,
          superview: this,
          x: pos.x,
          y: pos.y,
          width: tileWidth,
          height: tileHeight,
        });

        this.tiles[tileId] = tile;

        // make sure the generated tiles dont have a match 3
        while (this.haveMatch(tile)) {
          tile.randomisedTile();
        }
      }
    }
  };

  this._generateTilesRowFromBottom = function () {
    this.interactive = false;

    this._startTile = undefined;

    var cols = this.cols;
    var rows = this.rows;
    var newRow = rows - 1;

    var tileWidth = this.tileWidth;
    var tileHeight = this.tileHeight;

    // generate new tiles
    for (var col = 0; col < cols; col += 1) {
      // move old tiles up
      for (var row = 1; row < rows; row += 1) {
        var moveTargetTile = this.getTileByRowCol(row, col);

        // do nothing if there is no tile
        if (!moveTargetTile) {
          continue;
        }

        this.moveTileTo(moveTargetTile, row - 1, col);

        var oldPos = { y: this.getTilePixelPosByRowCol(row, col).y };
        var newPos = { y: this.getTilePixelPosByRowCol(row - 1, col).y };

        Helpers.animateMoveTile(moveTargetTile, oldPos, newPos);
      }

      this.setMaxListeners(cols * rows + cols);

      // generate new tile
      var tileId = Helpers.generateId(newRow, col);
      var pixelPos = this.getTilePixelPosById(tileId);

      var tile = new Tile({
        id: tileId,
        row: newRow,
        col: col,
        superview: this,
        x: pixelPos.x,
        y: pixelPos.y,
        width: tileWidth,
        height: tileHeight,
        scale: 0
      });

      this.tiles[tileId] = tile;

      // make sure the generated tiles dont have a match 3
      while (this.haveMatch(tile)) {
        tile.randomisedTile();
      }

      // show new tile
      tile.appear();

      // check first row, if there is tile, game over
      var firstRowTile = this.getTileByRowCol(0, col);

      if (firstRowTile) {
        this.interactive = false;
        this.emit('gameOver');
      }
    }

    this.sound.play('push');

    this.interactive = true;
  };


  /**
   * Setup swap tile input.
   */
  this._setupInput = function () {
    this.on('InputSelect', function (e) {
      if (!this.interactive || !this._startTile) {
        return;
      }

      // stop the board interaction
      this.interactive = false;

      // calculate swap direction
      var endSrcPoint = e.srcPoint;
      var xDiff = endSrcPoint.x - this._startSrcPoint.x;
      var yDiff = endSrcPoint.y - this._startSrcPoint.y
      var direction;

      if (Math.abs(xDiff) > Math.abs(yDiff)) {
        // right or left
        direction = xDiff > 0 ? 'right' : 'left';
      } else {
        // top or bottom
        direction = yDiff > 0 ? 'bottom' : 'top';
      }

      // need 2 tiles to swap, and they cannot be the same
      this.emit('validateSwap', direction);
    });

    this.on('InputStart', function (e) {
      if (!this.interactive) {
        return;
      }

      this._startTile = e.target;
      this._startSrcPoint = e.srcPoint;
    });
  };


  /**
   * Destroy tile and remove it from the board.
   * @param {Tile} tile - Tile to be destroy.
   * @param {callback} [cb]
   */
  this.destroyTile = function (tile, cb) {
    var that = this;

    this.tilesEffect[tile.id].gemId = tile.gemId;

    // animate disappear
    tile.disappear(function () {
      // remove reference from map
      that.tiles[tile.id] = undefined;

      // remove element from board
      tile.removeFromSuperview();

      that.tilesEffect[tile.id].gemId = undefined;

      if (cb) {
        cb();
      }
    });
  };


  /**
   * Swap Ids of two tiles in the tiles map, also row and col grid pos.
   * @param {Tile} tile1 - Tile 1.
   * @param {Tile} tile2 - Tile 2.
   */
  this.swapTileIds = function (tile1, tile2) {
    var newId = tile2.id;
    var newRow = tile2.row;
    var newCol = tile2.col;

    tile2.id = tile1.id;
    tile2.row = tile1.row;
    tile2.col = tile1.col;

    tile1.id = newId;
    tile1.row = newRow;
    tile1.col = newCol;

    this.tiles[tile1.id] = tile1;
    this.tiles[tile2.id] = tile2;
  };


  /**
   * Move tile to new grid pos.
   * @param {Tile} tile - Move target.
   * @param {number} row
   * @param {number} col
   */
  this.moveTileTo = function (tile, row, col) {
    this.tiles[tile.id] = undefined;

    tile.id = Helpers.generateId(row, col);
    tile.row = row;
    tile.col = col;

    this.tiles[tile.id] = tile;
  };


  /**
   * Check of tile have match with the same type with its neighbouring tiles.
   * It is a match if there is at least 3 same type including self.
   * Either directly adjacent with 1 tile or 2 tiles apart.
   * @param {Tile} tile - Tile.
   * @returns {boolean} Have match result flag.
   */
  this.haveMatch = function (tile) {
    var gemId = tile.gemId;

    var topTileId = Helpers.getTileNeighbourTileId(tile, 'top');
    var btmTileId = Helpers.getTileNeighbourTileId(tile, 'bottom');
    var leftTileId = Helpers.getTileNeighbourTileId(tile, 'left');
    var rightTileId = Helpers.getTileNeighbourTileId(tile, 'right');

    var topTile = this.getTileById(topTileId);
    var btmTile = this.getTileById(btmTileId);
    var leftTile = this.getTileById(leftTileId);
    var rightTile = this.getTileById(rightTileId);

    // matched vertically
    if (topTile && gemId === topTile.gemId && btmTile && gemId === btmTile.gemId) {
      return true;
    }

    // matched horizontally
    if (leftTile && gemId === leftTile.gemId && rightTile && gemId === rightTile.gemId) {
      return true;
    }

    var top2TileId = Helpers.getTileNeighbourTileId(tile, 'top', 2);
    var btm2TileId = Helpers.getTileNeighbourTileId(tile, 'bottom', 2);
    var left2TileId = Helpers.getTileNeighbourTileId(tile, 'left', 2);
    var right2TileId = Helpers.getTileNeighbourTileId(tile, 'right', 2);

    var top2Tile = this.getTileById(top2TileId);
    var btm2Tile = this.getTileById(btm2TileId);
    var left2Tile = this.getTileById(left2TileId);
    var right2Tile = this.getTileById(right2TileId);

    // matched top vertically
    if (topTile && gemId === topTile.gemId && top2Tile && gemId === top2Tile.gemId) {
      return true;
    }

    // matched bottom vertically
    if (btmTile && gemId === btmTile.gemId && btm2Tile && gemId === btm2Tile.gemId) {
      return true;
    }

    // matched left horizontally
    if (leftTile && gemId === leftTile.gemId && left2Tile && gemId === left2Tile.gemId) {
      return true;
    }

    // matched right horizontally
    if (rightTile && gemId === rightTile.gemId && right2Tile && gemId === right2Tile.gemId) {
      return true;
    }

    return false;
  };


  /**
   * Populate an array with a list of same type tiles.
   * @param {string[]} list - An empty array or contains tile Id.
   * @param {Tile} tile - Target tile.
   */
  this._populateListWithSimiliarNeighbouringTiles = function (list, tile) {
    if (list.indexOf(tile) === -1) {
      list.push(tile);
    }

    var gemId = tile.gemId;

    var topTileId = Helpers.getTileNeighbourTileId(tile, 'top');
    var btmTileId = Helpers.getTileNeighbourTileId(tile, 'bottom');
    var leftTileId = Helpers.getTileNeighbourTileId(tile, 'left');
    var rightTileId = Helpers.getTileNeighbourTileId(tile, 'right');

    var topTile = this.getTileById(topTileId);
    var btmTile = this.getTileById(btmTileId);
    var leftTile = this.getTileById(leftTileId);
    var rightTile = this.getTileById(rightTileId);

    if (topTile && list.indexOf(topTile) === -1 && gemId === topTile.gemId) {
      this._populateListWithSimiliarNeighbouringTiles(list, topTile);
    }

    if (btmTile && list.indexOf(btmTile) === -1 && gemId === btmTile.gemId) {
      this._populateListWithSimiliarNeighbouringTiles(list, btmTile);
    }

    if (leftTile && list.indexOf(leftTile) === -1 && gemId === leftTile.gemId) {
      this._populateListWithSimiliarNeighbouringTiles(list, leftTile);
    }

    if (rightTile && list.indexOf(rightTile) === -1 && gemId === rightTile.gemId) {
      this._populateListWithSimiliarNeighbouringTiles(list, rightTile);
    }
  };


  /**
   * Drop all the tiles to occupied empty spaces and check for subsequent combo.
   * @param {Tile[]} tiles - An array of tiles that is gone.
   * @param {boolean} noPower - Exlclude power reward.
   */
  this._dropTilesAndCheckComboMatch = function (tiles, noPower) {
    var colsBound = {};

    // calculate the lowest row for each cols and generate a map out of it
    for (var i = 0; i < tiles.length; i++) {
      var tile = tiles[i];

      if (!colsBound[tile.col]) {
        colsBound[tile.col] = 0;
      }

      colsBound[tile.col] = Math.max(colsBound[tile.col], tile.row);
    }

    // start to drop all the tiles to the lowest possible row taking up all the empty slot
    var movedTiles = [];

    for (var col in colsBound) {
      if (!colsBound.hasOwnProperty(col)) {
        continue;
      }

      col = col >> 0;

      var currentLowestRow = colsBound[col];
      var currentRow = currentLowestRow;

      var a = currentLowestRow

      // loop through each rows
      while (currentRow > 0) {
        var tile = this.getTileByRowCol(currentRow - 1, col);

        if (tile) {
          var oldTileId = tile.id;
          this.moveTileTo(tile, currentLowestRow, col);

          Helpers.animateMoveTile(
            tile,
            { y: this.getTilePixelPosById(oldTileId).y },
            { y: this.getTilePixelPosById(tile.id).y }
          );

          movedTiles.push(tile);

          currentLowestRow -= 1;
        }

        currentRow -= 1;
      }
    }

    // check all moved tiles for matches after the drop finished
    var that = this;

    var tileInfos = [];

    for (var i = 0; i < movedTiles.length; i += 1) {
      var tile = movedTiles[i];

      tileInfos.push({
        tile: tile,
        haveMatch: that.haveMatch(tile),
        tileId: tile.id
      });
    }

    this._chainCount++;

    // update score
    this.updateScore(this._chainCount * tiles.length);

    // trigger next match (combo)
    animate(this).wait(300).then(function () {
      that.emit('matched', tileInfos, noPower);
    });
  };

  /**
   * Update total score and emit event.
   * @param {number} score
   */
  this.updateScore = function (score) {
    this.totalScore += score;
    this.emit('updateScore', this.totalScore);
  };
});
