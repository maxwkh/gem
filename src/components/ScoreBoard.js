import device;
import ui.ImageScaleView  as ImageScaleView ;
import ui.resource.Image as Image;
import ui.TextView as TextView;

var scoreBoardImage = new Image({ url: 'resources/images/ui/scoreBoard.png' });

exports = Class(ImageScaleView , function (supr) {
  this.init = function (options) {
    options = options || {};

    if (!options.superview) {
      return console.log('ERROR Please supply a superview during the score board creation');
    }

    options = merge(options, {
      layout: 'box',
      autoSize: true,
      image: scoreBoardImage
    });

    supr(this, 'init', [options]);

    var font = 60;
    this.text = new TextView({
      superview: this,
      text: 0,
      size: font,
      color: '#ffffff',
      shadowColor: '#999999',
      horizontalAlign: 'center'
    });

    var that = this;

    // get the correct scaling based on device width
    scoreBoardImage.doOnLoad(function () {
      var imageWidth = scoreBoardImage.getSourceWidth();
      var imageHeight = scoreBoardImage.getSourceHeight();
      var width = device.width * 0.3;
      var scale = width / imageWidth;

      that.updateOpts({
        width: imageWidth,
        height: imageHeight,
        scale: scale,
        x: device.width - width - 20
      });

      that.text.updateOpts({
        y: imageHeight * 0.45,
        width: imageWidth,
        height: font,
      });
    });
  };

  this.updateScore = function (score) {
    this.text.setText(score);
  };
});