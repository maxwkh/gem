import animate;
import device;
import src.helpers.index as Helpers;
import ui.ImageScaleView as ImageScaleView;
import ui.ImageView as ImageView;
import ui.TextView as TextView;
import src.helpers.sounds as sounds;

exports = Class(ImageScaleView , function (supr) {
  this.init = function (options) {
    options = options || {};

    if (!options.superview) {
      return console.log('ERROR Please supply a superview during the button creation');
    }

    this.count = 0;

    this.board = options.board;

    var width = device.width * 0.2;
    var height = width * 0.8;

    this.sounds = sounds.getSound();

    options = merge(options, {
      width: width,
      height: height,
      x: width * 0.2,
      y: height * 0.2,
      layout: 'box',
      image: 'resources/images/ui/button.png',
      anchorX: width / 2,
      anchorY: height / 2,
    });

    supr(this, 'init', [options]);


    var iconWidth = width * 0.4;
    new ImageView({
      superview: this,
      width: iconWidth,
      height: iconWidth,
      image: 'resources/images/ui/power.png',
      x: width * 0.1,
      y: (height - iconWidth) / 2
    });

    this.text = new TextView({
      superview: this,
      text: this.count,
      color: '#ffffff',
      shadowColor: '#999999',
      horizontalAlign: 'center',
      x: width * 0.5,
      y: (height - iconWidth) / 2,
      width: iconWidth,
      height: iconWidth
    });

    this.setupInput();
  };

  this.setupInput = function () {
    var that = this;

    this.on('InputSelect', function () {
      var board = that.board;

      if (!board) {
        return;
      }

      if (!board.interactive) {
        return;
      }

      if (that.count <= 0) {
        return;
      }

      that.sounds.play('power');

      that.incPowerCount(-1);

      var tiles = board.tiles;
      var gemId = Helpers.getTypeWithHighestCount(tiles);
      var sameTiles = Helpers.extractTilesByGemId(tiles, gemId);

      board.destroyMatchedTilesAndCheckCombo(sameTiles, true);
    });
  };

  this.buttonAnimation = function () {
    if (this.count <= 0) {
      animate(this).now({ scale: 1 });
      return;
    }

    animate(this)
    .now({ scale: 1 })
    .then({ scale: 0.9 })
    .then(this.buttonAnimation);
  };

  this.incPowerCount = function (incCount) {
    this.count += incCount || 1;

    this.buttonAnimation();

    this.text.setText(this.count);
  };

  this.resetValue = function () {
    this.count = 0;
    this.text.setText(this.count);
  };
});