import animate;
import src.config as Config;
import ui.View as View;
import ui.ParticleEngine as ParticleEngine;

exports = Class(View, function (supr) {
  this.init = function (options) {
    options = options || {};

    if (!options.superview) {
      return console.log('ERROR Please supply a superview during the tile effect creation');
    }

    this.scale = options.scale || 1;

    this.width = options.width;
    this.height = options.height;
    this.gemId = undefined;

    supr(this, 'init', [options]);

    // tile particle effect
    this.effect = new ParticleEngine({
      superview: this
    });
  };

  this._emitParticles = function () {
    var data = this.effect.obtainParticleArray(1);
    var pObj = data[0];
    var tileDimensions = this.getPosition();

    pObj.dx = Math.random() * this.width * 2 - this.width;
    pObj.dy = Math.random() * this.height * 2 - this.height;
    pObj.width = this.width;
    pObj.height = this.height;
    pObj.image = 'resources/images/particles/gleam_0' + this.gemId +'.png',
    pObj.ttl = Config.particle.tileEffectTTL;

    this.effect.emitParticles(data);
  };

  this.tick = function (dt) {
    if (this.gemId) {
      this._emitParticles();
    }

    this.effect.runTick(dt);
  };
});
